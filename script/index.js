// Store data of api
let starships_details = []
let current_page = 1
let number_pages = 1

// clear previous content
function clear_content() {
    document.getElementById("detail-list-item").innerHTML = ""

    document.getElementById("detail-title").innerHTML = ""

    document.getElementById("starships-list-item").innerHTML = ""
}


// Get data from api with page
function get_data_from_api(page_number) {
    let url = "http://swapi.dev/api/starships/?page=" + page_number

    // Call the fetch function passing the url of the API as a parameter
    fetch(url)
        .then((resp) => resp.json()) // Transform the data into json
        .then((data)=>data["results"])
        .then(async function (data) {
            for (let i = 0; i < data.length; i++) {
                create_starship_item(i, data[i]["name"])
                data[i]['film-titles'] = await get_film_titles(data[i]['films'])
                starships_details[data[i]["name"]] = data[i]
            }
        })
        .catch((rejected)=>window.alert(rejected))
}


// Create a item for a starship name
function create_starship_item(id, name){
    let new_element = document.createElement("li")
    new_element.setAttribute("id", "starships-item-"+id)
    new_element.onclick = function() {
        show_detail_of_starship(name)
    }

    let new_a = document.createElement("a")
    new_element.appendChild(new_a)

    let new_text = document.createTextNode(name)
    new_a.appendChild(new_text)
    new_a.setAttribute("href", "javascript:void(0);")

    let parent = document.getElementById("starships-list-item")
    parent.appendChild(new_element)

}

// Show detail of a starship
function show_detail_of_starship(name){
    document.getElementById("detail-title").innerHTML = name

    let info = starships_details[name]
    let model = info["model"]
    let manufacturer = info["manufacturer"]
    let cost_in_credits = info["cost_in_credits"]
    let length = info["length"]
    let max_atmosphering_speed = info["max_atmosphering_speed"]
    let crew = info["crew"]
    let passengers = info["passengers"]
    let cargo_capacity = info["cargo_capacity"]
    let consumables = info["consumables"]
    let hyperdrive_rating = info["hyperdrive_rating"]
    let MGLT = info["MGLT"]
    let starship_class = info["starship_class"]
    let pilots = info["pilots"]

    let parent = document.getElementById("detail-list-item")
    parent.innerHTML = ''

    let element = document.createElement("li")
    element.appendChild(document.createTextNode(model))
    parent.appendChild(element)

    element = document.createElement("li")
    element.appendChild(document.createTextNode(manufacturer))
    parent.appendChild(element)

    element = document.createElement("li")
    element.appendChild(document.createTextNode(crew))
    parent.appendChild(element)

    element = document.createElement("li")
    element.appendChild(document.createTextNode(passengers))
    parent.appendChild(element)

    let film_titles = info["film-titles"]

    for (let i=0 ; i<film_titles.length ; i++){
        element = document.createElement("li")
        element.appendChild(document.createTextNode(film_titles[i]))
        parent.appendChild(element)
    }
}

// Getting title of films from links
async function get_film_titles(list_film_links) {
    let titles = new Array(list_film_links.length)

    for (let i = 0; i < list_film_links.length; i++) {
        await fetch(list_film_links[i])
            .then((resp) => resp.json()) // Transform the data into json
            .then((data) => data["title"])
            .then(function (title) {
                titles[i] = title
            })

    }

    return titles
}


// calculate number pages
async function calculate_number_pages(){
    let url = "http://swapi.dev/api/starships/?page=1"
     // Call the fetch function passing the url of the API as a parameter
    await fetch(url)
        .then((resp) => resp.json()) // Transform the data into json
        .then((data)=>data["count"])
        .then(function (count) {
            number_pages = Math.ceil(count/10)
        })
        .catch((rejected)=>window.alert(rejected))
}

// set functions to buttons
function set_functions_to_buttons() {
    let element = document.getElementById("page-prev")
    element.onclick = page_down

    element = document.getElementById("page-next")
    element.onclick = page_up
}



// page up
function page_up(){
    if (current_page<number_pages){
        current_page ++
        update_page()
    }
}

//page down
function page_down(){
    if (current_page>0){
        current_page --
        update_page()
    }
}

// update web with new page
function update_page(){
    clear_content()
    document.getElementById("page-current").innerHTML = "Page " + current_page
    get_data_from_api(current_page)
}

calculate_number_pages()
set_functions_to_buttons()
update_page()